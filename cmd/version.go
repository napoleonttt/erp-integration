/*
 * Copyright (c) 2020. SmartOSC Solution team - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package cmd

import (
	"fmt"
	"strconv"
	"time"

	"github.com/spf13/cobra"
)

var (
	Version       = "0.0.1"
	GitCommitHash = "undefined"
	BuiltAt       = "undefined"
)

// versionCmd represents the version command.
// Version command will show the current version of the service, the Git commit abbrev hash, and the built date
var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Display version information of Atom8 app",
	Run: func(cmd *cobra.Command, args []string) {
		b, _ := strconv.ParseInt(BuiltAt, 10, 64)
		o := time.Unix(b, 0)
		output := fmt.Sprintf(` 
  	    Vend service:
		  Version: %s
		  Git commit: %s
		  Built: %s
		`, Version, GitCommitHash, o)
		fmt.Println(output)
	},
}

func init() {
	rootCmd.AddCommand(versionCmd)
}
