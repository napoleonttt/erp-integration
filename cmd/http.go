/*
 * Copyright (c) 2020. SmartOSC Solution team - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package cmd

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"

	"github.com/gogo/gateway"
	pbRunTime "github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/rs/cors"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"google.golang.org/grpc"

	"bitbucket.org/smart-tech-hub/bm-pbtypes-go/erp_integration"
	"bitbucket.org/smart-tech-hub/erp-integration/smart_log"
)

var httpCmd = &cobra.Command{
	Use:   "http",
	Short: "Start the vend-service REST gateway",
	Run:   runServeHTTPCmd,
}

func init() {
	httpCmd.Flags().StringP("system-http-address", "r", ":8086", "REST server listen address")
	httpCmd.Flags().StringP("system-grpc-backend", "b", "127.0.0.1:8085", "gRPC backend for REST gateway")

	_ = viper.BindPFlag("system-http-address", httpCmd.Flags().Lookup("system-http-address"))
	_ = viper.BindPFlag("system-grpc-backend", httpCmd.Flags().Lookup("system-grpc-backend"))

	serveCmd.AddCommand(httpCmd)
}

// runServeHTTPCmd serves REST gateway.
func runServeHTTPCmd(cmd *cobra.Command, args []string) {
	smart_log.Logger.WithFields(log.Fields{
		"service": "vend-service",
		"type":    "http",
	}).Info("starting vend-service REST gateway")
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	jsonpb := &gateway.JSONPb{
		EmitDefaults: true,
		Indent:       "  ",
		OrigName:     true,
	}
	pbMux := pbRunTime.NewServeMux(
		pbRunTime.WithMarshalerOption(pbRunTime.MIMEWildcard, jsonpb),
		// This is necessary to get error details properly
		// marshaled in unary requests.
		pbRunTime.WithProtoErrorHandler(pbRunTime.DefaultHTTPProtoErrorHandler),
	)
	opts := []grpc.DialOption{grpc.WithInsecure()}
	backend := viper.GetString("system-grpc-backend")
	mux := http.NewServeMux()

	err := erp_integration.RegisterVendAPIHandlerFromEndpoint(ctx, pbMux, backend, opts)
	if err != nil {
		panic(err)
	}

	err = erp_integration.RegisterVendIntegrationAPIHandlerFromEndpoint(ctx, pbMux, backend, opts)
	if err != nil {
		panic(err)
	}

	go func() {
		restAddr := viper.GetString("system-http-address")
		mux.HandleFunc("/version", func(w http.ResponseWriter, req *http.Request) {
			b, _ := strconv.ParseInt(BuiltAt, 10, 64)
			o := time.Unix(b, 0)
			type ModuleVersion struct {
				Module    string
				Version   string
				GitCommit string
				Built     string
			}
			var mVersion = ModuleVersion{
				Module:    "vend-service",
				Version:   Version,
				GitCommit: GitCommitHash,
				Built:     o.String(),
			}
			js, err := json.Marshal(mVersion)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			w.Header().Set("Content-Type", "application/json")
			_, _ = w.Write(js)
		})
		mux.Handle("/", pbMux)
		// Config cors
		httpHandler := cors.AllowAll().Handler(mux)

		srv := &http.Server{
			Addr:         restAddr,
			Handler:      httpHandler,
			IdleTimeout:  60 * time.Second,
			ReadTimeout:  15 * time.Second,
			WriteTimeout: 15 * time.Second,
		}
		err = srv.ListenAndServe()
		if err != nil {
			fmt.Println(err)
		}
		smart_log.Logger.WithFields(log.Fields{
			"service": "vend-service",
			"type":    "http",
		}).Info("vend REST gateway started")
	}()
	<-c
	smart_log.Logger.WithFields(log.Fields{
		"service": "vend",
		"type":    "http",
	}).Info("vend REST gateway graceful shutdown")
}
