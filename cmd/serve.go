/*
 * Copyright (c) 2020. SmartOSC Solution team - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package cmd

import (
	"github.com/spf13/cobra"
)

var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "Start the application",
	Run:   runServeCmd,
}

func init() {
	rootCmd.AddCommand(serveCmd)
}

// runServeCmd serves both gRPC and REST gateway.
// From now, this command will be used mainly for development.
func runServeCmd(cmd *cobra.Command, args []string) {
	err := cmd.Help()
	if err != nil {
		panic(err)
	}
}
