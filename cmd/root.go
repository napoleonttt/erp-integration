package cmd

import (
	"bitbucket.org/smart-tech-hub/erp-integration/smart_log"
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	homedir "github.com/mitchellh/go-homedir"
)

var cfgFile string

var rootCmd = &cobra.Command{
	Use:              "erp-integration",
	Short:            "ERP Integration Command line interface",
	TraverseChildren: true,
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	initConfiguration()
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.erp_integration.yaml)")
	rootCmd.PersistentFlags().String("log-mode", "json", "Default log mode")
	rootCmd.PersistentFlags().String("log-level", "info", "Default log level")

	// Bind Flags to Viper
	_ = viper.BindPFlag("system-vault-oauth2-token-path", rootCmd.Flags().Lookup("system-vault-oauth2-token-path"))
	_ = viper.BindPFlag("log-mode", rootCmd.PersistentFlags().Lookup("log-mode"))
	_ = viper.BindPFlag("log-level", rootCmd.PersistentFlags().Lookup("log-level"))
	smart_log.InitLogger()
}

func initConfig() {
	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
	} else {
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		viper.AddConfigPath(home)
		viper.SetConfigName(".erp_integration")
	}
	viper.SetEnvPrefix("erp_integration")
	replacer := strings.NewReplacer("-", "_")
	viper.SetEnvKeyReplacer(replacer)
	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
	smart_log.InitLogger()
}

func initConfiguration() {
	rootCmd.PersistentFlags().String("system-mode", "dev", "dev mode")

	rootCmd.Flags().String("system-vault-address", "http://127.0.0.1:8200", "vault server listen address")
	rootCmd.Flags().String("system-vault-token", "s.JUjYDspsayuMg5MqL6G6ZIG7", "vault token")
	rootCmd.Flags().String("system-vault-path", "/vault/secrets/token", "vault token path")
	rootCmd.Flags().String("system-vault-vend-oauth2-token-path", "vend/dev", "system-vault-vend-oauth2-token-path")

	rootCmd.PersistentFlags().String("vend-client-id", "GZ1oGG7SymQ5Q10ZTffp9VRsLleQ3HiY", "vend-client-id")
	rootCmd.PersistentFlags().String("vend-client-secret", "SIJUcJmhkk8KSwpF0ahRnvBxsqRd9zyb", "vend-client-secret")
	rootCmd.PersistentFlags().String("vend-redirect-uri", "https://92b6bca19049.ngrok.io/setting/integrate/vend", "vend-redirect-uri")

	//Bind flags to viper
	_ = viper.BindPFlag("system-mode", rootCmd.PersistentFlags().Lookup("system-mode"))

	_ = viper.BindPFlag("system-vault-address", rootCmd.Flags().Lookup("system-vault-address"))
	_ = viper.BindPFlag("system-vault-token", rootCmd.Flags().Lookup("system-vault-token"))
	_ = viper.BindPFlag("system-vault-path", rootCmd.Flags().Lookup("system-vault-path"))
	_ = viper.BindPFlag("system-vault-oauth2-token-path", rootCmd.Flags().Lookup("system-vault-oauth2-token-path"))

	_ = viper.BindPFlag("vend-client-id", rootCmd.PersistentFlags().Lookup("vend-client-id"))
	_ = viper.BindPFlag("vend-client-secret", rootCmd.PersistentFlags().Lookup("vend-client-secret"))
	_ = viper.BindPFlag("vend-redirect-uri", rootCmd.PersistentFlags().Lookup("vend-redirect-uri"))
}