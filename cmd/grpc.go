/*
 * Copyright (c) 2020. SmartOSC Solution team - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package cmd

import (
	"bitbucket.org/smart-tech-hub/erp-integration/smart_log"
	"io/ioutil"
	"net"
	"os"
	"os/signal"
	"strings"
	"syscall"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"bitbucket.org/smart-tech-hub/erp-integration/app/domain/service"
	"bitbucket.org/smart-tech-hub/erp-integration/app/domain/usecase"
	app_service "bitbucket.org/smart-tech-hub/erp-integration/app/infrastructure/grpc"
	oauth2_config "bitbucket.org/smart-tech-hub/erp-integration/app/infrastructure/oauth2-config"
	vault_client "bitbucket.org/smart-tech-hub/erp-integration/app/infrastructure/vault-client"
)

// grpcCmd represents the grpc command
var grpcCmd = &cobra.Command{
	Use:   "grpc",
	Short: "Start the vend service",
	Run:   runServeGRPCCommand,
}

func init() {
	grpcCmd.Flags().StringP("system-grpc-address", "a", ":8085", "gRPC server listen address")

	_ = viper.BindPFlag("system-grpc-address", grpcCmd.Flags().Lookup("system-grpc-address"))

	serveCmd.AddCommand(grpcCmd)
}

func runServeGRPCCommand(cmd *cobra.Command, args []string) {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	vendClientID := viper.GetString("vend-client-id")
	vendClientSecret := viper.GetString("vend-client-secret")
	vendRedirectUri := viper.GetString("vend-redirect-uri")
	vendOAuthConfig := oauth2_config.NewOauth2Config(vendClientID, vendClientSecret, vendRedirectUri, []string{""})

	vaultAddr := viper.GetString("system-vault-address")
	var vaultToken string
	if viper.GetString("system-mode") == "local" {
		vaultToken = viper.GetString("system-vault-token")
	} else {
		vaultPath := viper.GetString("system-vault-path")
		tokenData, err := ioutil.ReadFile(vaultPath)
		if err != nil {
			panic(err)
		}

		vaultToken = strings.TrimSuffix(string(tokenData), "\n")
		if vaultToken == "" {
			panic("vault token misssing")
		}
	}
	vaultClient, err := vault_client.NewVaultClient(vaultAddr, vaultToken)
	if err != nil {
		panic(err)
	}
	oauth2TokenSecretPath := viper.GetString("system-vault-oauth2-token-path")
	services := service.NewService(vaultClient, vendOAuthConfig, oauth2TokenSecretPath)

	usecases := usecase.NewUseCase(services)

	grpcServer := app_service.InitGrpcServer(usecases)

	grpcAddr := viper.GetString("system-grpc-address")
	lis, err := net.Listen("tcp", grpcAddr)
	if err != nil {
		panic(err)
	}
	go func() {
		smart_log.Logger.WithFields(log.Fields{
			"service": "vend-service",
			"type":    "grpc",
		}).Info("vend-service app started")
		err = grpcServer.Serve(lis)
		if err != nil {
			panic(err)
		}
	}()

	<-c
	smart_log.Logger.WithFields(log.Fields{
		"service": "vend-service",
		"type":    "grpc",
	}).Info("vend-service app graceful shutdown")
}
