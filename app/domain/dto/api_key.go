/*
 * Copyright (c) 2020. SmartOSC Solution team - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package dto

import (
	"path/filepath"
)

func generateVaultPath(secretPath string, tenantID string) string {
	return filepath.Join(secretPath, tenantID)
}

type SaveAPIKeyDto struct {
	SecretPath string
	TenantID   string
	Key        string
}

func (t SaveAPIKeyDto) GetVaultPath() string {
	return generateVaultPath(t.SecretPath, t.TenantID)
}

type GetAPIKeyDto struct {
	SecretPath string
	TenantID   string
}

func (t GetAPIKeyDto) GetVaultPath() string {
	return generateVaultPath(t.SecretPath, t.TenantID)
}
