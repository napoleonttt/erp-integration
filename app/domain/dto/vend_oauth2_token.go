/*
 * Copyright (c) 2020. SmartOSC Solution team - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package dto

import (
	"encoding/json"
	"fmt"
	"golang.org/x/oauth2"
	"strings"
)

//const VendServicePlatform = "Vend_service"

type SaveVendOauth2TokenDto struct {
	TenantID    string
	ProductCode string
	Token       *oauth2.Token
}

func generateSecretPath(oauth2TokenSecretPath string, productCode string) string {
	productCodeLowerCase := strings.ToLower(productCode)
	return fmt.Sprintf("%v/%v", productCodeLowerCase, oauth2TokenSecretPath)
}

func (t *SaveVendOauth2TokenDto) ConvertToApiKey(oauth2TokenSecretPath string) (*SaveAPIKeyDto, error) {
	tokenBytes, err := json.Marshal(t.Token)
	if err != nil {
		return nil, err
	}
	return &SaveAPIKeyDto{
		SecretPath: generateSecretPath(oauth2TokenSecretPath, t.ProductCode),
		TenantID:   t.TenantID,
		Key:        string(tokenBytes),
	}, nil
}

type GetVendOauth2TokenDto struct {
	TenantID    string
	ProductCode string
}

func (t *GetVendOauth2TokenDto) ConvertToApiKey(oauth2TokenSecretPath string) *GetAPIKeyDto {
	return &GetAPIKeyDto{
		SecretPath: generateSecretPath(oauth2TokenSecretPath, t.ProductCode),
		TenantID:   t.TenantID,
	}
}
