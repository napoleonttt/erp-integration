package dto

type GetAuthCodeResponse struct {
	Code    string `json:"code"`
	Message string `json:"message"`
}

type ExchangeTokenResponse struct {
	AccessToken  string `json:"access_token"`
	TokenType    string `json:"token_type"`
	Expires      uint32 `json:"expires"`
	ExpiresIn    uint32 `json:"expires_in"`
	RefreshToken string `json:"refresh_token"`
	DomainPrefix string `json:"domain_prefix"`
}

type ExchangeTokenDTO struct {
	Domain      string
	AuthCode    string
	TenantID    string
	ProductCode string
}
