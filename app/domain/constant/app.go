package constant

const (
	BCRLRequestsLeft  = "X-Rate-Limit-Requests-Left"
	BCRLRequestsQuota = "X-Rate-Limit-Requests-Quota"
	BCRLTimeResetMs   = "X-Rate-Limit-Time-Reset-Ms"
	BCRLTimeWindowMs  = "X-Rate-Limit-Time-Window-Ms"
)
