/*
 * Copyright (c) 2020. SmartOSC Solution team - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package constant

import "errors"

var (
	ErrPlatformAPIKeyNotFound = errors.New("email platform's api key not found")
)
