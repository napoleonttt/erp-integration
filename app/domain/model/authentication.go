/*
 * Copyright (c) 2020. SmartOSC Solution team <solution@smartosc.com> - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package model

type AuthenticationResult struct {
	AccessToken  string
	ExpiresIn    int
	IdToken      string
	RefreshToken string
	TokenType    string
}
