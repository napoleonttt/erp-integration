/*
 * Copyright (c) 2020. SmartOSC Solution team - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package model

import "golang.org/x/oauth2"

type VendOauth2Token struct {
	TenantID string
	Token    *oauth2.Token
}
