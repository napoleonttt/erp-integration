package usecase

import "bitbucket.org/smart-tech-hub/erp-integration/app/domain/service"

type UseCase struct {
	VendUC VendUseCaseInterface
}

func NewUseCase(services *service.Service) *UseCase {
	vendUC := NewVendUseCase(services.VendAuthServiceInterface)
	return &UseCase{
		VendUC: vendUC,
	}
}