package usecase

import (
	"bitbucket.org/smart-tech-hub/erp-integration/app/domain/dto"
	"bitbucket.org/smart-tech-hub/erp-integration/app/domain/service"
)

type VendUseCaseInterface interface {
	ExchangeAccessToken(exchangeDto *dto.ExchangeTokenDTO) error
	GetIntegrationUrl() (string, error)
}

type VendUseCase struct {
	vendAuthService service.VendAuthServiceInterface
}

func NewVendUseCase(vendAuthService service.VendAuthServiceInterface) *VendUseCase {
	return &VendUseCase{
		vendAuthService: vendAuthService,
	}
}

func (v *VendUseCase) ExchangeAccessToken(exchangeDto *dto.ExchangeTokenDTO) error {
	return v.vendAuthService.ExchangeAccessToken(exchangeDto)
}

func (v *VendUseCase) GetIntegrationUrl() (string, error) {
	return v.vendAuthService.GenerateFrontEndIntegrateURL()
}