/*
 * Copyright (c) 2020. SmartOSC Solution team <solution@smartosc.com> - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package service

import (
	"encoding/json"

	"golang.org/x/oauth2"

	"bitbucket.org/smart-tech-hub/erp-integration/app/domain/dto"
	"bitbucket.org/smart-tech-hub/erp-integration/app/domain/model"
)

type VendOauth2TokenServiceInterface interface {
	SaveVendOauth2Token(saveTokenDto *dto.SaveVendOauth2TokenDto) error
	GetVendOauth2Token(getTokenDto *dto.GetVendOauth2TokenDto) (*model.VendOauth2Token, error)
	RemoveVendOauth2Token(getTokenDto *dto.GetVendOauth2TokenDto) error
}

type VendOauth2TokenService struct {
	apiKeyService         ApiKeyServiceInterface
	oauth2TokenSecretPath string
}

func NewVendOauth2TokenService(apiKeyService ApiKeyServiceInterface, oauth2TokenSecretPath string) *VendOauth2TokenService {
	return &VendOauth2TokenService{
		apiKeyService:         apiKeyService,
		oauth2TokenSecretPath: oauth2TokenSecretPath,
	}
}

func (t *VendOauth2TokenService) SaveVendOauth2Token(saveTokenDto *dto.SaveVendOauth2TokenDto) error {
	saveApiKeyDto, err := saveTokenDto.ConvertToApiKey(t.oauth2TokenSecretPath)
	if err != nil {
		return err
	}
	return t.apiKeyService.SaveAPIKey(saveApiKeyDto)
}

func (t *VendOauth2TokenService) GetVendOauth2Token(getTokenDto *dto.GetVendOauth2TokenDto) (*model.VendOauth2Token, error) {
	tokenStr, err := t.apiKeyService.GetAPIKey(getTokenDto.ConvertToApiKey(t.oauth2TokenSecretPath))
	if err != nil {
		return nil, err
	}
	var token oauth2.Token
	err = json.Unmarshal([]byte(tokenStr), &token)
	if err != nil {
		return nil, err
	}
	return &model.VendOauth2Token{
		TenantID: getTokenDto.TenantID,
		Token:    &token,
	}, nil
}

func (t *VendOauth2TokenService) RemoveVendOauth2Token(getTokenDto *dto.GetVendOauth2TokenDto) error {
	return t.apiKeyService.RemoveAPIKey(getTokenDto.ConvertToApiKey(t.oauth2TokenSecretPath))
}
