package service

import (
	vault_client "bitbucket.org/smart-tech-hub/erp-integration/app/infrastructure/vault-client"
	"golang.org/x/oauth2"
)

type Service struct {
	ApiKeyService                   ApiKeyServiceInterface
	VendAuthServiceInterface        VendAuthServiceInterface
	VendOauth2TokenServiceInterface VendOauth2TokenServiceInterface
}

func NewService(
	vaultClient *vault_client.VaultClient,
	oauth2Config *oauth2.Config,
	oauth2TokenSecretPath string,
) *Service {
	apiKeyService := NewApiKeyService(vaultClient)
	vendOauth2TokenService := NewVendOauth2TokenService(apiKeyService, oauth2TokenSecretPath)
	vendAuthService := NewVendAuthService(vendOauth2TokenService, oauth2Config)
	return &Service{
		ApiKeyService:                   apiKeyService,
		VendAuthServiceInterface:        vendAuthService,
		VendOauth2TokenServiceInterface: vendOauth2TokenService,
	}
}
