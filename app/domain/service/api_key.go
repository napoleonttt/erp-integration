/*
 * Copyright (c) 2020. SmartOSC Solution team <solution@smartosc.com> - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package service

import (
	"github.com/sirupsen/logrus"

	"bitbucket.org/smart-tech-hub/erp-integration/app/domain/constant"
	"bitbucket.org/smart-tech-hub/erp-integration/app/domain/dto"
	vaultclient "bitbucket.org/smart-tech-hub/erp-integration/app/infrastructure/vault-client"
	"bitbucket.org/smart-tech-hub/erp-integration/smart_log"
)

const Key = "key"

type ApiKeyServiceInterface interface {
	SaveAPIKey(apiKey *dto.SaveAPIKeyDto) error
	GetAPIKey(getApiKeyDto *dto.GetAPIKeyDto) (string, error)
	RemoveAPIKey(getApiKeyDto *dto.GetAPIKeyDto) error
}

type ApiKeyService struct {
	vaultClient *vaultclient.VaultClient
}

func NewApiKeyService(vaultClient *vaultclient.VaultClient) *ApiKeyService {
	return &ApiKeyService{
		vaultClient: vaultClient,
	}
}

func (t *ApiKeyService) SaveAPIKey(apiKey *dto.SaveAPIKeyDto) error {
	data := map[string]interface{}{
		Key: apiKey.Key,
	}

	kv := vaultclient.NewVaultKv(apiKey.GetVaultPath(), data)
	return t.vaultClient.SaveKV(kv)
}

func (t *ApiKeyService) GetAPIKey(getApiKeyDto *dto.GetAPIKeyDto) (string, error) {
	logger := smart_log.Logger.WithFields(logrus.Fields{
		"service":     "ApiKeyService.GetAPIKey",
		"secret_path": getApiKeyDto.SecretPath,
		"tenant_id":   getApiKeyDto.TenantID,
	})
	path := getApiKeyDto.GetVaultPath()
	data, err := t.vaultClient.ReadKV(path)
	if err != nil {
		logger.WithField("path", path).WithError(err).Error("failed to read secret")
		return "", err
	}
	if data == nil {
		logger.WithField("path", path).WithError(constant.ErrPlatformAPIKeyNotFound).Error("api key not found")
		return "", constant.ErrPlatformAPIKeyNotFound
	}

	key, ok := data[Key].(string)
	if !ok {
		logger.WithField("path", path).WithError(constant.ErrPlatformAPIKeyNotFound).Error("api key not found")
		return "", constant.ErrPlatformAPIKeyNotFound
	}

	return key, nil
}

func (t *ApiKeyService) RemoveAPIKey(getApiKeyDto *dto.GetAPIKeyDto) error {
	logger := smart_log.Logger.WithFields(logrus.Fields{
		"service":     "ApiKeyService.RemoveAPIKey",
		"secret_path": getApiKeyDto.SecretPath,
		"tenant_id":   getApiKeyDto.TenantID,
	})
	_, err := t.GetAPIKey(getApiKeyDto)
	if err != nil {
		logger.WithError(err).Error("failed to get api key")
		return err
	}
	return t.vaultClient.DeleteKV(getApiKeyDto.GetVaultPath())
}
