package service

import (
	"bitbucket.org/smart-tech-hub/erp-integration/app/domain/dto"
	"context"
	"fmt"
	"golang.org/x/oauth2"
)

type VendAuthServiceInterface interface {
	ExchangeAccessToken(exchangeDto *dto.ExchangeTokenDTO) error
	GenerateFrontEndIntegrateURL() (string, error)
}

type VendAuthService struct {
	vendOauth2TokenService VendOauth2TokenServiceInterface
	oauth2Config           *oauth2.Config
}

func NewVendAuthService(
	vendOauth2TokenService VendOauth2TokenServiceInterface,
	oauth2Config *oauth2.Config,
) *VendAuthService {
	return &VendAuthService{
		vendOauth2TokenService: vendOauth2TokenService,
		oauth2Config:           oauth2Config,
	}
}

func (v *VendAuthService) ExchangeAccessToken(exchangeDto *dto.ExchangeTokenDTO) error {
	tokenUrl := fmt.Sprintf(v.oauth2Config.Endpoint.TokenURL, exchangeDto.Domain)
	v.oauth2Config.Endpoint.TokenURL = tokenUrl
	token, err := v.oauth2Config.Exchange(context.TODO(), exchangeDto.AuthCode)
	if err != nil {
		return err
	}
	return v.vendOauth2TokenService.SaveVendOauth2Token(&dto.SaveVendOauth2TokenDto{
		TenantID:    exchangeDto.TenantID,
		ProductCode: exchangeDto.ProductCode,
		Token:       token,
	})
}

func (v *VendAuthService) GenerateFrontEndIntegrateURL() (string, error) {
	u := v.oauth2Config.AuthCodeURL("xyz")
	return u, nil
}
