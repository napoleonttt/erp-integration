package grpc

import (
	"google.golang.org/grpc"

	"bitbucket.org/smart-tech-hub/bm-pbtypes-go/erp_integration"
	"bitbucket.org/smart-tech-hub/erp-integration/app/domain/usecase"
)

func InitGrpcServer(usecase *usecase.UseCase) *grpc.Server {
	integrationServiceServer := NewIntegrationServiceServer(usecase.VendUC)
	vendServiceServer := NewVendServiceServer(usecase.VendUC)
	grpcServer := grpc.NewServer()

	erp_integration.RegisterVendIntegrationAPIServer(grpcServer, integrationServiceServer)
	erp_integration.RegisterVendAPIServer(grpcServer, vendServiceServer)
	return grpcServer
}
