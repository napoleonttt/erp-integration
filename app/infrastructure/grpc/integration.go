package grpc

import (
	"bitbucket.org/smart-tech-hub/bm-pbtypes-go/erp_integration"
	"bitbucket.org/smart-tech-hub/erp-integration/app/domain/dto"
	"bitbucket.org/smart-tech-hub/erp-integration/app/domain/usecase"
	"context"
)

type IntegrationServiceServer struct {
	vendUseCase usecase.VendUseCaseInterface
}

func NewIntegrationServiceServer(vendUC usecase.VendUseCaseInterface) *IntegrationServiceServer {
	return &IntegrationServiceServer{
		vendUseCase: vendUC,
	}
}

func (i *IntegrationServiceServer) ExchangeToken(ctx context.Context, req *erp_integration.ExchangeTokenRequest) (*erp_integration.ExchangeTokenResponse, error) {
	err := i.vendUseCase.ExchangeAccessToken(&dto.ExchangeTokenDTO{
		AuthCode:    req.AuthCode,
		TenantID:    req.TenantId,
		ProductCode: req.ProductCode,
		Domain:      req.DomainName,
	})
	if err != nil {
		return nil, err
	}
	return &erp_integration.ExchangeTokenResponse{
		Code: "0",
		Message: "Success",
	}, nil
}
