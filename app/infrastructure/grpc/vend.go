package grpc

import (
	"bitbucket.org/smart-tech-hub/bm-pbtypes-go/erp_integration"
	"bitbucket.org/smart-tech-hub/erp-integration/app/domain/usecase"
	"context"
)

type VendServiceServer struct {
	vendUseCase usecase.VendUseCaseInterface
}

func NewVendServiceServer(vendUC usecase.VendUseCaseInterface) *VendServiceServer {
	return &VendServiceServer{
		vendUseCase: vendUC,
	}
}

func (v *VendServiceServer) GetIntegrationUrl(ctx context.Context, req *erp_integration.GetIntegrationUrlRequest) (*erp_integration.GetIntegrationUrlResponse, error) {
	url, err := v.vendUseCase.GetIntegrationUrl()
	if err != nil {
		return nil, err
	}
	return &erp_integration.GetIntegrationUrlResponse{
		Code:           "0",
		Message:        "Success",
		IntegrationUrl: url,
	}, nil
}
