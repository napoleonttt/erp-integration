package oauth2_config

import (
	"golang.org/x/oauth2"
)

const (
	authURL = "https://secure.vendhq.com/connect"
	tokenURI = "https://%v.vendhq.com/api/1.0/token"
)

func NewOauth2Config(clientID string, clientSecret string, redirectURL string, scopes []string) *oauth2.Config {
	return &oauth2.Config{
		ClientID:     clientID,
		ClientSecret: clientSecret,
		RedirectURL:  redirectURL,
		Scopes:       scopes,
		Endpoint: oauth2.Endpoint{
			AuthURL:  authURL,
			TokenURL: tokenURI,
		},
	}
}
