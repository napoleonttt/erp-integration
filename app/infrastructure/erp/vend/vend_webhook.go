package vend

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"sync"

	"github.com/sirupsen/logrus"
)

var Logger = logrus.New()

func RegisterWebhook(personalToken string, domainPrefix, webhookEndpoint string) error {
	registerWebhookUrl := fmt.Sprintf(VendWebhookURL, domainPrefix)
	logger := Logger.WithFields(logrus.Fields{
		"service": "OauthService.RegisterWebhook",
	})

	logger.WithFields(logrus.Fields{"web_hook_url": registerWebhookUrl, "web_hook_endpoint": webhookEndpoint}).Infof("webhook register requesting")

	errChan := make(chan error)
	var wg sync.WaitGroup
	wg.Add(len(WebhookEvents))
	for _, scope := range WebhookEvents {
		tmpScope := scope
		go func() {
			errChan <- registerWebhook(tmpScope, webhookEndpoint, registerWebhookUrl, personalToken, domainPrefix)
			wg.Done()
		}()
	}

	go func() {
		wg.Wait()
		close(errChan)
	}()

	for {
		err, ok := <-errChan
		if !ok {
			break
		}
		if err != nil {
			return err
		}
	}
	logger.Info("All webhooks registered successfully")

	return nil
}

func registerWebhook(webhookType string, webhookEndpoint string, registerWebHooksURL string, personalToken string, domainPrefix string) error {
	logger := Logger.WithFields(logrus.Fields{
		"usecase":       "OauthUseCase.RegisterWebHooks",
		"type":          webhookType,
		"domain_prefix": domainPrefix,
	})
	logger.Info("Registering webhooks")
	payload, err := json.Marshal(map[string]interface{}{
		"url":    webhookEndpoint,
		"active": true,
		"type":   webhookType,
	})
	if err != nil {
		logger.WithError(err).Error("error marshalling webhooks event")
	}
	data := url.Values{}
	data.Set("data", string(payload))
	req, err := http.NewRequest("POST", registerWebHooksURL, strings.NewReader(data.Encode()))
	if err != nil {
		logger.WithField("data", data).WithError(err).Error("error creating registering webhook request")
		return err
	}
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Authorization", "Bearer "+personalToken)

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		logger.WithError(err).Error("Error registering webhook")
		return err
	}
	resBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logger.WithFields(logrus.Fields{
			"type":          webhookType,
			"domain_prefix": domainPrefix,
		}).WithError(err).Error("Error reading webhook registering respond")
		return err
	}

	if resp.StatusCode != http.StatusOK {
		Logger.WithFields(logrus.Fields{
			"status_code": resp.StatusCode,
			"resp_body":   string(resBytes),
		}).Error("webhook register failed")
	} else {
		logger.WithFields(logrus.Fields{
			"type":          webhookType,
			"domain_prefix": domainPrefix,
		}).Info("webhook registered")
	}
	return nil
}
