package vend

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
	"net/url"

	"bitbucket.org/smart-tech-hub/erp-integration/app/domain/constant"
	"bitbucket.org/smart-tech-hub/erp-integration/app/domain/dto"
	"bitbucket.org/smart-tech-hub/erp-integration/smart_log"
)

func GetAuthCode(ctx context.Context, queries map[string]string) (*dto.GetAuthCodeResponse, error) {
	logger := smart_log.Logger.WithFields(logrus.Fields{
		"platform": "vend",
		"method":   "GetAuthCode",
	})

	authCodeURL := AuthURL
	var u *url.URL
	u, err := url.Parse(authCodeURL)
	if err != nil {
		logger.WithError(err).Error("parse url failed")
		return nil, err
	}
	var q url.Values
	q, err = url.ParseQuery(u.RawQuery)
	if err != nil {
		logger.WithError(err).Error("parse url query failed")
		return nil, err
	}
	for key, val := range queries {
		q.Set(key, val)
	}
	u.RawQuery = q.Encode()

	var req *http.Request
	req, err = GetApiRequest(u.String())
	if err != nil {
		logger.WithError(err).Error("prepare api request to vend failed")
		return nil, err
	}

	client := &http.Client{}
	var resp *http.Response
	resp, err = client.Do(req)
	if err != nil {
		logger.WithError(err).Error("send request to vend failed")
		return nil, err
	}
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logger.WithField("body", resp.Body).WithError(err).Error("read vend response body failed")
		return nil, err
	}
	defer func() {
		err = resp.Body.Close()
		if err != nil {
			smart_log.Logger.Error(err)
		}
	}()
	if resp.StatusCode != http.StatusOK {
		// BC rate limit handle
		if resp.StatusCode == http.StatusTooManyRequests {
			timeResetMs := resp.Header.Get(constant.BCRLTimeResetMs)
			err = errors.New(fmt.Sprintf("429 Too Many Requests. X-Rate-Limit-Time-Reset-Ms: %s", timeResetMs))
			return nil, err
		}
		if resp.StatusCode != http.StatusNoContent {
			LogVendError("get authorization code", u.String(), "", resp.StatusCode, bodyBytes)
			err = errors.New("failed to get authorization code")
			return nil, err
		}
		return nil, err
	}

	authCodeResponse := &dto.GetAuthCodeResponse{}
	err = json.Unmarshal(bodyBytes, authCodeResponse)
	if err != nil {
		logger.Error("unmarshal vend response failed")
		return nil, err
	}
	return authCodeResponse, nil
}

func ExchangeToken(ctx context.Context, domainPrefix string, queries map[string]string) (*dto.ExchangeTokenResponse, error) {
	logger := smart_log.Logger.WithFields(logrus.Fields{
		"platform": "vend",
		"method":   "ExchangeToken",
	})

	exchangeTokenURL := fmt.Sprintf(ExchangeTokenURL, domainPrefix)
	var u *url.URL
	u, err := url.Parse(exchangeTokenURL)
	if err != nil {
		logger.WithError(err).Error("parse url failed")
		return nil, err
	}

	var req *http.Request
	inputJson, err := json.Marshal(queries)
	if err != nil {
		logger.WithField("data", queries).WithError(err).Error("parse request body to json failed")
		return nil, err
	}
	req, err = PostApiRequest(bytes.NewBuffer(inputJson), u.String())
	if err != nil {
		logger.WithError(err).Error("prepare api request to vend failed")
		return nil, err
	}
	client := &http.Client{}

	var resp *http.Response
	resp, err = client.Do(req)
	if err != nil {
		logger.WithError(err).Error("send request to vend failed")
		return nil, err
	}

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logger.WithField("body", resp.Body).WithError(err).Error("read vend response body failed")
		return nil, err
	}
	defer func() {
		err = resp.Body.Close()
		if err != nil {
			smart_log.Logger.Error(err)
		}
	}()
	if resp.StatusCode != http.StatusOK {
		// BC rate limit handle
		if resp.StatusCode == http.StatusTooManyRequests {
			timeResetMs := resp.Header.Get(constant.BCRLTimeResetMs)
			err = errors.New(fmt.Sprintf("429 Too Many Requests. X-Rate-Limit-Time-Reset-Ms: %s", timeResetMs))
			return nil, err
		}
		if resp.StatusCode != http.StatusNoContent {
			LogVendError("get access token", u.String(), "", resp.StatusCode, bodyBytes)
			err = errors.New("failed to get access token")
			return nil, err
		}
		return nil, err
	}

	exchangeResponse := &dto.ExchangeTokenResponse{}
	err = json.Unmarshal(bodyBytes, exchangeResponse)
	if err != nil {
		logger.Error("unmarshal vend response failed")
		return nil, err
	}
	//err = errors.New(fmt.Sprintf("429 Too Many Requests. X-Rate-Limit-Time-Reset-Ms: %s", "5000"))
	return exchangeResponse, err
}
