package vend

var (
	WebhookEvents = map[string]string{
		"ProductUpdate": "product.update",
		"InventoryUpdate": "inventory.update",
	}
)

const (
	VendWebhookURL = "https://%v.vendhq.com/api/webhooks"
)