package vend

import (
	"io"
	"net/http"

	"bitbucket.org/smart-tech-hub/erp-integration/smart_log"
)

const (
	AuthURL = "https://secure.vendhq.com/connect"
	ExchangeTokenURL = "https://%v.vendhq.com/api/1.0/token"
	ApiURL = "https://%v.vendhq.com/api/"
)

func GetApiRequest(requestURL string) (*http.Request, error) {
	return ApiRequest("GET", nil, requestURL)
}

func PostApiRequest(body io.Reader, requestURL string) (*http.Request, error) {
	return ApiRequest("POST", body, requestURL)
}

func PutApiRequest(body io.Reader, requestURL string) (*http.Request, error) {
	return ApiRequest("PUT", body, requestURL)
}

func DeleteApiRequest(requestURL string) (*http.Request, error) {
	return ApiRequest("DELETE", nil, requestURL)
}

func ApiRequest(method string, body io.Reader, requestURL string) (*http.Request, error) {
	smart_log.Logger.WithField("method", method).Info("Requesting: ", requestURL)
	req, err := http.NewRequest(method, requestURL, body)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")
	return req, nil
}

func LogVendError(action string, url string, input interface{}, responseCode int, responseBody []byte) {
	smart_log.Logger.WithField("action", action).
		WithField("url", url).
		WithField("input", input).
		Errorf("response code: %v, body: %v", responseCode, string(responseBody))
}