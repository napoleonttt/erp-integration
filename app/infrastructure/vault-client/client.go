/*
 * Copyright (c) 2020. SmartOSC Solution team - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package vault_client

import (
	"strings"

	vault "github.com/hashicorp/vault/api"
	"github.com/sirupsen/logrus"

	"bitbucket.org/smart-tech-hub/erp-integration/smart_log"
)

type VaultClient struct {
	client *vault.Client
}

func NewVaultClient(addr, token string) (*VaultClient, error) {
	cfg := vault.DefaultConfig()
	client, err := vault.NewClient(cfg)
	if err != nil {
		return nil, err
	}

	err = client.SetAddress(addr)
	if err != nil {
		return nil, err
	}
	client.SetToken(token)

	return &VaultClient{
		client: client,
	}, nil
}

func (v *VaultClient) SaveKV(kv *VaultKV) error {
	logger := smart_log.Logger.WithFields(logrus.Fields{
		"service": "VaultClient.SaveKV",
	})
	logical := v.client.Logical()

	_, err := logical.Write(strings.ToLower(kv.path), kv.data)
	if err != nil {
		logger.WithError(err).Error("failed to save secret")
		return err
	}

	return nil
}

func (v *VaultClient) ReadKV(path string) (map[string]interface{}, error) {
	logger := smart_log.Logger.WithFields(logrus.Fields{
		"service": "VaultClient.ReadKV",
		"path":    path,
	})
	logical := v.client.Logical()

	sec, err := logical.Read(strings.ToLower(path))
	if err != nil {
		logger.WithError(err).Error("failed to read secrets")
		return nil, err
	}

	if sec == nil {
		return nil, nil
	}

	return sec.Data, nil
}

func (v *VaultClient) DeleteKV(path string) error {
	logger := smart_log.Logger.WithFields(logrus.Fields{
		"service": "VaultClient.DeleteKV",
		"path":    path,
	})
	logical := v.client.Logical()

	sec, err := logical.Delete(strings.ToLower(path))
	if err != nil {
		logger.WithError(err).Error("failed to delete secrets")
		return err
	}

	if sec == nil {
		return nil
	}

	return nil
}
