/*
 * Copyright (c) 2020. SmartOSC Solution team - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package vault_client

type VaultKV struct {
	path string
	data map[string]interface{}
}

func NewVaultKv(path string, data map[string]interface{}) *VaultKV {
	return &VaultKV{
		path: path,
		data: data,
	}
}