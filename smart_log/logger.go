package smart_log

import (
	"github.com/sirupsen/logrus"

	smart_logger "bitbucket.org/smart-tech-hub/smart-logger"
)

var Logger *logrus.Logger

func InitLogger() {
	Logger = smart_logger.NewLogger(nil)
}
