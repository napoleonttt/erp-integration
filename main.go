package main

import "bitbucket.org/smart-tech-hub/erp-integration/cmd"

func main() {
	cmd.Execute()
}
